#include "stdafx.h"
#include "AIComponents.h"


void AIPaddleController::onEnterScene()
{
	auto spriteComp = m_entity->getComponent<sf2d::comps::Sprite>();
	assert(spriteComp);
	spriteComp->setSize(PADDLE_SIZE);
	spriteComp->setColour(sf2d::Colour::Red);
	reset();
}

void AIPaddleController::tick()
{
}

void AIPaddleController::reset()
{
	sf2d::Vec2f pos;
	pos.x = (GET_APP()->getRenderWindow()->getSize().x) - (PADDLE_SIZE.x / 2.0f) - 10.0f;
	pos.y = static_cast<float>(GET_APP()->getRenderWindow()->getSize().y) / 2.0f;
	m_entity->transform->setPosition(pos);
}