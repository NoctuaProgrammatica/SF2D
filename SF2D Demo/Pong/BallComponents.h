#pragma once

#include <SF2D/Application.h>
#include <SFML/Graphics/CircleShape.hpp>

// Ball Shape
class BallShape :
	public sf2d::comps::RenderableBase
{
public:

	BallShape(sf2d::Entity* entity, float ballRadius);
	~BallShape() = default;

	virtual sf2d::InitStatus init() override;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual sf2d::Rectf getOnscreenBounds() const { return sf2d::Rectf(); }

private:

	sf::CircleShape m_shape;
	float m_radius;
};


// Ball Controller
class BallController :
	public sf2d::ComponentBase
{
public:

	BallController(sf2d::Entity* entity, float ballRadius);
	~BallController() = default;

	virtual sf2d::InitStatus init() override;
	virtual void onEnterScene() override;
	void reset();
	virtual void tick() override;

private:

	void collisionCb(const sf2d::comps::CollisionDetectionData& data);
	sf2d::comps::ColliderBaseCallback m_cbFunc = [this](const sf2d::comps::CollisionDetectionData& data) -> void
	{
		collisionCb(data);
	};

	const float BALL_SPEED = 250.0f;

	float m_radius;

	
	sf2d::Vec2f m_direction;
};
