#include <SF2D/Render/Renderer.h>
#include <SF2D/Application.h>

#include <algorithm>

#include <SFML/Graphics.hpp>
#include <SF2D/AssetLoader/AssetLoader.h>

using namespace sf2d;
using namespace sf2d::render;

using namespace std;

Renderer::Renderer()
	: m_renderingType(RendererType::Default), m_sortType(RenderSortType::None)
{

}

Renderer::~Renderer()
{
	m_screenText.clear();
}

void Renderer::render()
{
	sf::RenderWindow* const target = Application::getApp()->getRenderWindow();
	//target->setActive(true);

	//while (target->isOpen())
	{

		target->clear();
		defaultRender();

		for (auto& text : m_screenText)
		{
			sf::Text t(text.second.getString(), mp_defaultFont);
			t.setCharacterSize(text.second.getCharacterSize());
			t.setPosition(screenToWorld(text.first));
			target->draw(t);
		}

		for (auto& func : m_lastDrawCallbacks)
		{
			func();
		}
		target->display();
	}
}

void Renderer::generateRenderableList()
{

	m_renderablesVector.clear();
	m_splitMapVec.clear();
	auto const pCurrentScene = Application::getApp()->getCurrentScene();
	KCHECK(pCurrentScene);
	auto entityList = pCurrentScene->getEntityList();

	for (int i = 0; i < CHUNK_POOL_SIZE; ++i)
	{
		if (!entityList[i].allocated)
		{
			continue;
		}

		Entity& entity = entityList[i].entity;
		if (!entity.isActive())
		{
			continue;
		}
		//auto pSprite = entity.getComponent<comps::KCRenderableBase>();
		auto pSpriteList = entity.getComponents<comps::RenderableBase>();
		if (pSpriteList.size() == 0)
		{
			continue;
		}

		for (auto& s : pSpriteList)
		{
			m_renderablesVector.push_back(s);
		}
	}


	for (auto renderable : m_renderablesVector)
	{
		KCHECK(renderable);
		comps::TileMapSplit* pSplitMap = dynamic_cast<comps::TileMapSplit*>(renderable);
		if (pSplitMap)
		{
			m_splitMapVec.push_back(pSplitMap);
		}
	}

	for (auto pSplit : m_splitMapVec)
	{
		auto& mapLines = pSplit->getHorizontalTileLines();
		for (auto& line : mapLines)
		{
			m_renderablesVector.push_back((comps::RenderableBase*)(&line));
		}
		m_renderablesVector.erase(std::find(m_renderablesVector.begin(), m_renderablesVector.end(), pSplit));
	}
}

void Renderer::sortByRenderLayer()
{
	std::sort(m_renderablesVector.begin(), m_renderablesVector.end(), [](comps::RenderableBase* objA, comps::RenderableBase* objB)
		{
			return objA->getRenderLayer() < objB->getRenderLayer();
		});
}

void sf2d::render::Renderer::sortByZOrder()
{
	std::sort(m_renderablesVector.begin(), m_renderablesVector.end(), [](comps::RenderableBase* objA, comps::RenderableBase* objB) -> bool
		{
			auto objABounds = objA->getOnscreenBounds();
			auto objBBounds = objB->getOnscreenBounds();

			return (objABounds.top + objABounds.height) < (objBBounds.top + objBBounds.height);
		});
}

void Renderer::defaultRender()
{
	sf::RenderWindow* const target = Application::getApp()->getRenderWindow();
	generateRenderableList();
	switch (m_sortType)
	{
	case RenderSortType::ZOrderSort:
		sortByZOrder(); //@Remember to do impl for this function
		break;
	case RenderSortType::LayerSort:
		sortByRenderLayer();
	case RenderSortType::None:
	default:
		break;
	}

	for (auto& pSplit : m_splitMapVec)
	{
		target->draw(*pSplit);
	}

	for (auto& renderable : m_renderablesVector)
	{
		target->draw(*renderable);
		renderable->postRenderEvent();
	}

	if (m_bShowDebugDrawables)
	{
		for (auto& s : m_debugShapes)
		{
			target->draw(*s);
		}
	}
}

Vec2f Renderer::screenToWorld(const Vec2i& vec) const
{
	return Application::getApp()->getRenderWindow()->mapPixelToCoords(vec);
}

void Renderer::subscribeLastDrawCallback(std::function<void(void)> func)
{
	m_lastDrawCallbacks.push_back(func);
}

void Renderer::removeDebugShape(sf::Drawable* pShape)
{
	auto result = find_if(m_debugShapes.begin(), m_debugShapes.end(), [pShape](sf::Drawable* ele) -> bool
		{
			return ele == pShape;
		});

	if (result != m_debugShapes.end())
	{
		m_debugShapes.erase(result);
	}
}
