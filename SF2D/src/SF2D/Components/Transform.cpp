#include <SF2D/Components/Transform.h>
#include <SF2D/Maths/MathVector.h>
#include <SF2D/Components/PhysicsBody.h>
#include <SF2D/Application.h>

using namespace sf2d;
using namespace sf2d::comps;

Transform::Transform(Entity* pEntity)
	: ComponentBase(pEntity), m_pParentTransform(nullptr), m_bHasParent(false),
	m_rotation(0.0f), m_origin(0.0f, 0.0f), m_scale(1.0f, 1.0f), m_trans(0.0, 0.0f)
{

}

Transform::Transform(Transform& toCopy)
	: ComponentBase(toCopy.m_entity), m_pParentTransform(toCopy.m_pParentTransform), m_bHasParent(toCopy.m_bHasParent),
	m_rotation(toCopy.m_rotation), m_origin(toCopy.m_origin), m_scale(toCopy.m_scale), m_trans(toCopy.m_trans), m_bUpdateTransform(true)

{
}

void Transform::operator=(Transform& toCopy)
{
	m_pParentTransform = toCopy.m_pParentTransform;
	m_bHasParent = toCopy.m_bHasParent;

	m_rotation = toCopy.m_rotation;
	m_origin = toCopy.m_origin;
	m_scale = toCopy.m_scale;
	m_trans = toCopy.m_trans;
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::tick()
{
	if (m_bUpdateTransform)
	{
		reconstructTransform();
		m_bUpdateTransform = false;
	}
	if (m_bHasParent)
	{
		//float parentRot = m_pParentTransform->getRotation();
		m_combinedWithParentTransform = m_pParentTransform->getTransform() * m_transform;
		//m_parentedTransform.translate(m_pParentTransform->getOrigin().x*m_pParentTransform->getScale().x, m_pParentTransform->getOrigin().y*m_pParentTransform->getScale().y);
	}

	m_worldRot = getRotation();
	m_worldPos = getPosition();
	m_worldScale = getScale();
}

const sf::Transform& sf2d::comps::Transform::getTransform() const
{
	if (m_bHasParent)
	{
		return m_combinedWithParentTransform;
	}
	return m_transform;
}

void Transform::setParent(Entity* pEntity)
{
	Transform* pTransform = pEntity->getComponent<Transform>();
	KCHECK(pTransform);
	m_bHasParent = true;
	m_pParentTransform = pTransform;
	m_combinedWithParentTransform = m_pParentTransform->getTransform() * m_transform;
}

void sf2d::comps::Transform::setRotation(float angleInDeg)
{
	m_rotation = angleInDeg;
	if (fabs(m_rotation) > 360.0f)
	{
		m_rotation = fmod(m_rotation, 360.0f);
	}

	auto body = m_entity->getComponent<PhysicsBody>();
	if (body)
	{
		body->setRotation(m_rotation);
	}
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::setOrigin(const Vec2f& origin)
{
	m_origin = origin;
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::setOrigin(float x, float y)
{
	setOrigin(Vec2f(x, y));
}

void sf2d::comps::Transform::setPosition(const Vec2f& trans)
{
	m_trans = trans;
	auto body = m_entity->getComponent<PhysicsBody>();
	if (body)
	{
		body->setPosition(trans);
	}
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::setPosition(float dx, float dy)
{
	setPosition(Vec2f(dx, dy));
}

const Vec2f sf2d::comps::Transform::getPosition() const
{
	const sf::Transform* const pTransform = !m_bHasParent ? &m_transform : &m_combinedWithParentTransform;
	if (!m_bHasParent)
	{
		return pTransform->transformPoint(getOrigin());
	}
	else
	{
		return pTransform->transformPoint(getOrigin() + m_trans);
	}
}

float sf2d::comps::Transform::getRotation() const
{
	if (!m_bHasParent)
	{
		return m_rotation;
	}

	return fmod(m_rotation + m_pParentTransform->getRotation(), 360.0f);
}

const Vec2f& sf2d::comps::Transform::getOrigin() const
{
	return m_origin;
}

void sf2d::comps::Transform::setScale(const Vec2f& scale)
{
	m_scale = scale;
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::setScale(float x, float y)
{
	setScale(Vec2f(x, y));
}

Vec2f sf2d::comps::Transform::getScale() const
{
	if (m_bHasParent)
		return Vec2f(m_scale.x * m_pParentTransform->getScale().x, m_scale.y * m_pParentTransform->getScale().y);
	return m_scale;
}

void sf2d::comps::Transform::move(float dx, float dy)
{
	move(Vec2f(dx, dy));
}

void sf2d::comps::Transform::move(const Vec2f& trans)
{
	setPosition(m_trans + trans);
	auto body = m_entity->getComponent<PhysicsBody>();
	if (body)
	{
		body->setPosition(m_trans + trans);
	}
	//m_bUpdateTransform = true;
	reconstructTransform();
}

void sf2d::comps::Transform::rotate(float angleInDeg)
{
	setRotation(m_rotation + angleInDeg);
	auto body = m_entity->getComponent<PhysicsBody>();
	if (body)
	{
		body->setRotation(m_rotation + angleInDeg);
	}
}

void sf2d::comps::Transform::scale(float sx, float sy)
{
	scale(Vec2f(sx, sy));
}

void sf2d::comps::Transform::scale(const Vec2f& scale)
{
	setScale(m_scale.x * scale.x, m_scale.y * scale.y);
}

void sf2d::comps::Transform::reconstructTransform()
{
	/*
	- Credit to SFML transformable class for how to create a transform component
	*/
	const float angleInRad = maths::Radians(-m_rotation);
	const float cosAngle = cosf(angleInRad);
	const float sinAngle = sinf(angleInRad);

	const float scaleSineX = m_scale.x * sinAngle, scaleSineY = m_scale.y * sinAngle;
	const float scaleCosX = m_scale.x * cosAngle, scaleCosY = m_scale.y * cosAngle;
	const float transX = -m_origin.x * scaleCosX - m_origin.y * scaleSineY + m_trans.x;
	const float transY = m_origin.x * scaleSineX - m_origin.y * scaleCosY + m_trans.y;

	m_transform = sf::Transform(scaleCosX, scaleSineY, transX, -scaleSineX, scaleCosY, transY,
		0.0f, 0.0f, 1.0f);
}
