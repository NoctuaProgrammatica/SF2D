#include <SF2D/Application.h>
#include <SF2D/Components/PhysicsBody.h>
#include <SF2D/Physics\b2dConversion.h>

#include <SF2D/Components/CircleCollider.h>
#include <SF2D/Components/BoxCollider.h>
#include <box2d/box2d.h>

using namespace sf2d;
using namespace sf2d::comps;

// Conversion Functions for data structures
b2BodyDef convertToB2BodyDef(const BodyDef& def)
{
	b2BodyDef bodyDef;
	bodyDef.type = KCAST(b2BodyType, def.bodyType);
	bodyDef.bullet = def.bBullet;
	bodyDef.gravityScale = def.gravityScale;
	bodyDef.awake = def.bAwake;
	bodyDef.allowSleep = def.bAllowSleep;
	bodyDef.angle = def.angle;
	bodyDef.position = Vec2fTob2(def.position);
	bodyDef.linearVelocity = Vec2fTob2(def.linearVelocity);
	bodyDef.angularVelocity = def.angularVelocity;
	bodyDef.enabled = def.bActive;
	bodyDef.fixedRotation = def.bFixedRotation;
	bodyDef.linearDamping = def.linearDamping;
	bodyDef.angularDamping = def.angularDamping;

	return bodyDef;
}

b2FixtureDef convertToB2FixtureDef(const MatDef& def)
{
	b2FixtureDef fixtureDef;
	fixtureDef.density = def.density;
	fixtureDef.friction = def.friction;
	fixtureDef.restitution = def.restitution;

	return fixtureDef;
}

// Class Functions

PhysicsBody::PhysicsBody(Entity& entity, const Vec2f& bounds)
	: ComponentBase(&entity), m_bounds(bounds), m_halfBounds(m_bounds * 0.5f)
{
}

PhysicsBody::PhysicsBody(Entity& entity, const Vec2f& bounds, const BodyDef& bodyDef)
	: ComponentBase(&entity), m_bounds(bounds), m_halfBounds(m_bounds * 0.5f), m_bodyDef(bodyDef)
{
}

PhysicsBody::PhysicsBody(Entity& entity, const Vec2f& bounds, const BodyDef& bodyDef, const MatDef& matDef)
	: ComponentBase(&entity), m_bounds(bounds), m_halfBounds(m_bounds * 0.5f), m_bodyDef(bodyDef), m_matDef(matDef)
{

}

InitStatus PhysicsBody::init()
{
	auto pApp = GET_APP();
	auto& physWorld = pApp->getPhysicsWorld();

	auto convertedBodyDef = convertToB2BodyDef(m_bodyDef);
	m_pB2Body = physWorld.addNewBody(convertedBodyDef);

	const float ppm = physWorld.getPPM();

	m_polygonShape.SetAsBox(m_halfBounds.x / ppm, m_halfBounds.y / ppm);
	b2FixtureDef fixtureDef = convertToB2FixtureDef(m_matDef);
	fixtureDef.shape = &m_polygonShape;
	m_pCurrentFixture = m_pB2Body->CreateFixture(&fixtureDef);

	return InitStatus::Success;
}

void PhysicsBody::fixedTick()
{
	const float ppm = GET_APP()->getPhysicsWorld().getPPM();
	const Vec2f translation = b2ToVec2f(m_pB2Body->GetPosition());
	const float rotation = maths::Degrees(m_pB2Body->GetAngle());
	m_entity->transform->setPosition(translation * ppm);
	m_entity->transform->setRotation(rotation);
}

void PhysicsBody::onEnterScene()
{
	auto pCollider = m_entity->getComponent<ColliderBase>();
	if (pCollider != nullptr)
	{
		m_pB2Body->DestroyFixture(m_pCurrentFixture);
		b2FixtureDef fixtureDef = convertToB2FixtureDef(m_matDef);
		fixtureDef.shape = pCollider->getB2Shape().lock().get();
		m_pCurrentFixture = m_pB2Body->CreateFixture(&fixtureDef);
	}
}

void PhysicsBody::onExitScene()
{
	auto& physWorld = GET_APP()->getPhysicsWorld();
	physWorld.removeBody(m_pB2Body);
	m_pB2Body = nullptr;
}

void PhysicsBody::setPosition(const Vec2f& position) const
{
	const float ppm = GET_APP()->getPhysicsWorld().getPPM();

	m_pB2Body->SetTransform(Vec2fTob2(position / ppm), m_pB2Body->GetAngle());
}

void PhysicsBody::setRotation(float rotation) const
{
	// Box2D requires angle in radians, but internally
	// we use degrees, so convert.
	const float rad = maths::Radians(rotation);
	m_pB2Body->SetTransform(m_pB2Body->GetPosition(), rad);
}

void PhysicsBody::setActivity(bool bIsActive) const
{
	m_pB2Body->SetEnabled(bIsActive);
}

Vec2f PhysicsBody::getLinearVelocity() const
{
	return Vec2f(b2ToVec2f(m_pB2Body->GetLinearVelocity()));
}

void PhysicsBody::setLinearVelocity(const Vec2f& velocity)
{
	m_pB2Body->SetLinearVelocity(Vec2fTob2(velocity));
}

float PhysicsBody::getAngularVelocity() const
{
	return m_pB2Body->GetAngularVelocity();
}

void PhysicsBody::setAngularVelocity(float velocity)
{
	m_pB2Body->SetAngularVelocity(velocity);
}

void PhysicsBody::applyForce(const Vec2f& force, const Vec2f& point, bool wake)
{
	m_pB2Body->ApplyForce(Vec2fTob2(force), Vec2fTob2(force), wake);
}

void PhysicsBody::applyForceToCentre(const Vec2f& force, bool wake)
{
	m_pB2Body->ApplyForceToCenter(Vec2fTob2(force), wake);
}

void PhysicsBody::applyTorque(float torque, bool wake)
{
	m_pB2Body->ApplyTorque(torque, wake);
}

void PhysicsBody::applyLinearImpulse(const Vec2f& impulse, const Vec2f& point, bool wake)
{
	m_pB2Body->ApplyLinearImpulse(Vec2fTob2(impulse), Vec2fTob2(point), wake);
}

void PhysicsBody::applyLinearImpulseToCenter(const Vec2f& impulse, bool wake)
{
	m_pB2Body->ApplyLinearImpulseToCenter(Vec2fTob2(impulse), wake);
}

void PhysicsBody::getMass() const
{
	m_pB2Body->GetMass();
}

float PhysicsBody::getGravityScale() const
{
	return m_pB2Body->GetGravityScale();
}

void PhysicsBody::setGravityScale(float scale)
{
	m_pB2Body->SetGravityScale(scale);
}

void PhysicsBody::setDensity(float density)
{
	m_pB2Body->GetFixtureList()->SetDensity(density);
}
