#include <SF2D/Components/ColliderBase.h>
#include <SF2D/Physics/b2dConversion.h>
#include <SF2D/Components/Transform.h>
#include <box2d/box2d.h>

using namespace sf2d;
using namespace sf2d::comps;

using namespace std;

//--Components::ColliderBase--\\ 
ColliderBase::ColliderBase(Entity* pEntity, ColliderType type, bool bUseParentLocation)
	: ComponentBase(pEntity), m_colliderType(type), m_collisionLayer(0), m_bUseParentLocation(bUseParentLocation)
{
	switch (type)
	{
		//case KCColliderType::Polygon:
	case ColliderType::AABB:
		m_pShape = make_shared<b2PolygonShape>();
		break;
	case ColliderType::Circle:
		m_pShape = make_shared<b2CircleShape>();
		break;
		//case KCColliderType::OBB:
		//	KPRINTF("OBB no longer a primitive type, consider using KCPolyCollider to achieve the same result\n");
		//	break;
	default:
		spdlog::debug("Unexpected behaviour within ColliderBase ctor");
	}
}

int32 ColliderBase::subscribeCollisionCallback(ColliderBaseCallback* callback)
{
	KCHECK(callback);
	if (!callback)
	{
		return -1; // if callback passed is null, return -1 to indicarte it wasn't added
	}

	m_callbacks.push_back(callback);
	return 0;
}

int32 ColliderBase::unsubscribeCollisionCallback(ColliderBaseCallback* callback)
{
	auto findResult = std::find(m_callbacks.begin(), m_callbacks.end(), callback);

	if (findResult == m_callbacks.end())
	{
		return -1; // if this call back isn't subscribed to the collider then return -1
	}

	m_callbacks.erase(findResult); // remove the callback from the vector
	return 0;
}

void ColliderBase::collisionCallback(const CollisionDetectionData& collData)
{
	for (auto& callback : m_callbacks)
	{
		(*callback)(collData);
	}
}

bool ColliderBase::isCallbackSubscribed(ColliderBaseCallback* callback) const
{
	auto findResult = std::find(m_callbacks.begin(), m_callbacks.end(), callback);

	return findResult != m_callbacks.end();
}

void ColliderBase::setCollisionFilteringData(const ColliderFilteringData& filteringPOD)
{
	m_filterData = filteringPOD;
}

b2Transform ColliderBase::getB2Transform()
{
	const float angleInRad = maths::Radians(m_entity->transform->getRotation());
	b2Vec2 position;
	if (m_bUseParentLocation)
	{
		position = Vec2fTob2(m_entity->transform->getTranslation());
	}
	else
	{
		position = Vec2fTob2(m_overridePosition);
	}
	return b2Transform(position, b2Rot(angleInRad));
}

