#include <SF2D/Components/BoxCollider.h>
#include <SF2D/Components/Transform.h>

#include <box2d/b2_polygon_shape.h>
#include <SF2D/Application.h>
#include <SF2D/Physics/b2dConversion.h>

using namespace sf2d;
using namespace sf2d::comps;

BoxCollider::BoxCollider(Entity* pEntity, const Vec2f& size, bool bUseParentLocation) :
	ColliderBase(pEntity, ColliderType::AABB, bUseParentLocation), m_size(size), m_halfSize(size * 0.5f)
{
	transform = m_entity->getComponent<Transform>();
	auto p = getB2Shape().lock();
	m_pBoxShapeb2 = std::dynamic_pointer_cast<b2PolygonShape>(p);
	auto ppm = GET_APP()->getPhysicsWorld().getPPM();
	m_pBoxShapeb2.lock()->SetAsBox(m_halfSize.x / ppm, m_halfSize.y / ppm);// , Vec2fTob2(Vec2f(0.0f, 0.0f)), 0.0f);
}

const Rectf& BoxCollider::getBounds()
{
	updateAABB();
	return m_aabb;
}

const Rectf& BoxCollider::getBoundingBox()
{
	updateAABB();
	return m_aabb;
}

Vec2f BoxCollider::getTopLeftCoord() const
{
	if (!isUsingParentLocation())
	{
		// This may cause issues if the box not using the override position is rotated
		// since we calculate the top-left coord without taking into account any rotation
		return getOverridePosition() - m_halfSize;
	}

	const Vec2f& origin = transform->getOrigin();
	const Vec2f pos = transform->getTransform().transformPoint(Vec2f(0.0f, 0.0f));
	return pos;
}

void BoxCollider::updateAABB()
{
	if (isUsingParentLocation())
	{
		float angle = transform->getRotation();
		m_aabb = transform->getTransform().transformRect(Rectf(Vec2f(0,0), m_size));
	}
	else
	{
		const Vec2f topLeft = getTopLeftCoord();
		m_aabb = Rectf(topLeft, m_size);
	}
}
