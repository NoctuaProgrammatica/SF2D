#include <SF2D/Components/AnimatedSprite.h>
#include <SF2D/Application.h>

#include <SFML/System/Time.hpp>
#include <SF2D/AssetLoader/AssetLoader.h>

using namespace sf2d;
using namespace sf2d::comps;
using namespace sf2d::anim;

AnimatedSprite::AnimatedSprite(sf2d::Entity* pEntity, AnimationDef* pAnimation)
	: RenderableBase(pEntity), m_bIsPlaying(false), m_bIsPaused(false), m_bRepeat(false), m_animTimer(0.0f), m_frameIdx(0), m_pCurrentAnim(pAnimation)
{
	m_verts = sf::VertexArray(sf::Quads, 4);
	m_pTransformComponent = pEntity->transform;

	if (!pAnimation)
	{
		return;
	}
	setupVertArray(false);
}

void AnimatedSprite::setSize(const Vec2f& size)
{
	m_verts[0].position = Vec2f(0.0f, 0.0f);
	m_verts[1].position = Vec2f(size.x, 0.0f);
	m_verts[2].position = Vec2f(size.x, size.y);
	m_verts[3].position = Vec2f(0.0f, size.y);
}

void AnimatedSprite::updateTextureRect(const Rectf& texRect)
{
	m_verts[0].texCoords = Vec2f(texRect.left, texRect.top);
	m_verts[1].texCoords = Vec2f(texRect.left + texRect.width, texRect.top);
	m_verts[2].texCoords = Vec2f(texRect.left + texRect.width, texRect.top + texRect.height);
	m_verts[3].texCoords = Vec2f(texRect.left, texRect.top + texRect.height);
}

void AnimatedSprite::setupVertArray(bool bUseOverrideSize)
{
	const Vec2f& bounds = m_pCurrentAnim->bounds;
	const Vec2f& frameData = m_pCurrentAnim->frameData[m_frameIdx];
	if (!bUseOverrideSize)
	{
		m_size = m_pCurrentAnim->bounds;
		m_verts[0].position = Vec2f(0.0f, 0.0f);
		m_verts[1].position = Vec2f(m_pCurrentAnim->bounds.x, 0.0f);
		m_verts[2].position = Vec2f(m_pCurrentAnim->bounds.x, m_pCurrentAnim->bounds.y);
		m_verts[3].position = Vec2f(0.0f, m_pCurrentAnim->bounds.y);
	}
	else
	{
		m_verts[0].position = Vec2f(0.0f, 0.0f);
		m_verts[1].position = Vec2f(m_size.x, 0.0f);
		m_verts[2].position = Vec2f(m_size.x, m_size.y);
		m_verts[3].position = Vec2f(0.0f, m_size.y);
	}

	const Vec2f topLeftTex = bUseOverrideSize ? Vec2f(m_size.x * frameData.x, m_size.y * frameData.y) : Vec2f(bounds.x * frameData.x, bounds.y * frameData.y);
	m_verts[0].texCoords = topLeftTex;
	m_verts[1].texCoords = Vec2f(topLeftTex.x + bounds.x, topLeftTex.y);
	m_verts[2].texCoords = Vec2f(topLeftTex.x + bounds.x, topLeftTex.y + bounds.y);
	m_verts[3].texCoords = Vec2f(topLeftTex.x, topLeftTex.y + bounds.y);

	for (auto i = 0; i < 4; ++i)
	{
		m_verts[i].color = Colour::White;
	}
}

void AnimatedSprite::draw(sf::RenderTarget& rTarget, sf::RenderStates rStates) const
{
	rStates.transform *= m_pTransformComponent->getTransform();
	rStates.shader = getShader();
	rStates.texture = m_pCurrentAnim->pTexture;
	rTarget.draw(m_verts, rStates);
}

void AnimatedSprite::tick()
{
	const float dt = Application::getApp()->getDeltaTime();
	if (!m_bIsPlaying || m_bIsPaused) // if anim is not playing don't handle any of tick states
	{
		return;
	}

	m_animTimer += dt;
	if (m_animTimer > m_pCurrentAnim->frameTime)
	{
		const sf::Time frameTime = sf::seconds(m_pCurrentAnim->frameTime);
		const sf::Time currentFrameTime = sf::seconds(m_animTimer);

		const sf::Time ModuloCurrentTime = sf::microseconds(currentFrameTime.asMicroseconds() % frameTime.asMicroseconds());
		m_animTimer = ModuloCurrentTime.asSeconds();

		if (m_frameIdx + 1 < m_pCurrentAnim->frameData.size())
		{
			setFrameIndex(m_frameIdx + 1);
		}
		else
		{
			if (m_bRepeat)
			{
				setFrameIndex(0);
			}
			else
			{
				m_bIsPlaying = false;
			}
			m_animTimer = 0.0f;
		}
	}
}

Rectf sf2d::comps::AnimatedSprite::getOnscreenBounds() const
{
	return m_pTransformComponent->getTransform().transformRect(Rectf(Vec2f(0.0f, 0.0f), m_pCurrentAnim->bounds));
}

void AnimatedSprite::play()
{
	m_bIsPlaying = true;
	m_bIsPaused = false;
}

void AnimatedSprite::pause()
{
	// m_bIsPlaying = false;
	m_bIsPaused = true;
}

void AnimatedSprite::stop()
{
	m_animTimer = 0.0f;
	m_bIsPlaying = false;
	setFrameIndex(0);
}

void AnimatedSprite::setFrameIndex(sf2d::int32 i)
{
	KCHECK(i < m_pCurrentAnim->frameData.size());
	m_frameIdx = i;
	Rectf rect;
	rect.width = m_pCurrentAnim->bounds.x;
	rect.height = m_pCurrentAnim->bounds.y;
	rect.left = m_pCurrentAnim->frameData[i].x * m_pCurrentAnim->bounds.x;
	rect.top = m_pCurrentAnim->frameData[i].y * m_pCurrentAnim->bounds.y;
	updateTextureRect(rect);
}

void AnimatedSprite::setAnimation(const std::string& animName, bool bUseOverrideSize)
{
	AnimationDef* pAnim = AssetLoader::getAssetLoader().getAnimation(animName);

	KCHECK(pAnim);

	m_pCurrentAnim = pAnim;
	setupVertArray(bUseOverrideSize);
	setFrameIndex(0);
	m_animTimer = 0.0f;
}
