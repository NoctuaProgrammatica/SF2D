#include <SF2D/Components/CircleCollider.h>
#include <SF2D/Components/Transform.h>
#include <SF2D/Application.h>

#include <box2d/b2_circle_shape.h>


using namespace sf2d;
using namespace sf2d::comps;

CircleCollider::CircleCollider(Entity* pEntity, float radius, bool bUseParentLocation)
	: ColliderBase(pEntity, ColliderType::Circle, bUseParentLocation), m_radius(radius)

{
	transform = pEntity->getComponent<Transform>();
	auto p = getB2Shape().lock();
	m_pCircleShape = std::dynamic_pointer_cast<b2CircleShape>(p);
	auto ppm = GET_APP()->getPhysicsWorld().getPPM();
	m_pCircleShape.lock()->m_radius = m_radius / ppm;
}

const Vec2f& CircleCollider::getCentrePosition()
{
	updateCentrePosition();
	return m_centrePos;
}

float sf2d::comps::CircleCollider::getRadius() const
{
	return m_radius;
}

const Rectf& sf2d::comps::CircleCollider::getBoundingBox()
{
	updateCentrePosition();
	constexpr float mutliplier = 2.0f;
	m_boundingBox.left = m_centrePos.x - m_radius * mutliplier;
	m_boundingBox.top = m_centrePos.y - m_radius * mutliplier;
	m_boundingBox.height = m_boundingBox.width = 2 * (m_radius * mutliplier);
	return m_boundingBox;
}

void sf2d::comps::CircleCollider::updateCentrePosition()
{
	Vec2f topLeft;

	topLeft = transform->getPosition();

	if (transform->getOrigin() == Vec2f(m_radius, m_radius))
	{
		m_centrePos = transform->getTransform().transformPoint(m_radius, m_radius);
		return;
	}

	m_centrePos = topLeft + Vec2f(m_radius, m_radius);
}
