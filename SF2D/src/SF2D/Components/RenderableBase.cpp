#include <SF2D/Components/RenderableBase.h>
#include <SF2D/AssetLoader/AssetLoader.h>

using namespace sf2d;
using namespace sf2d::comps;


RenderableBase::RenderableBase(sf2d::Entity* pEntity)
	: ComponentBase(pEntity)
{
	this->setShader(ASSET().getShader("default_shader"));
}

