#include <SF2D/Components/Sprite.h>
#include <SF2D/AssetLoader/AssetLoader.h>

#include <SFML/Graphics.hpp>

using namespace sf2d;
using namespace sf2d::comps;

Sprite::Sprite(Entity * pEntity, const Vec2f& size)
	:RenderableBase(pEntity), m_size(size), m_pTexture(nullptr), transform(nullptr)
{
	m_vertexArray = sf::VertexArray(sf::Quads, 4);

	m_vertexArray[0].position = Vec2f(0.0f, 0.0f);
	m_vertexArray[1].position = Vec2f(m_size.x, 0.0f);
	m_vertexArray[2].position = Vec2f(m_size.x, m_size.y);
	m_vertexArray[3].position = Vec2f(0.0f, m_size.y);

	m_vertexArray[0].color = Colour::White;
	m_vertexArray[1].color = Colour::White;
	m_vertexArray[2].color = Colour::White;
	m_vertexArray[3].color = Colour::White;

	transform = m_entity->transform;
	setTexture(ASSET().getTexture("missing"));
}

void Sprite::draw(sf::RenderTarget & rTarget, sf::RenderStates rStates) const
{
	rStates.transform *= transform->getTransform();
	rStates.texture = m_pTexture;
	rStates.shader = getShader();
	rTarget.draw(m_vertexArray, rStates);
}

SF2D_API void Sprite::setColour(const Colour & col)
{
	for (int i = 0; i < 4; ++i)
	{
		m_vertexArray[i].color = col;
	}
}

void Sprite::setTexture(sf::Texture * pTexture)
{
	m_pTexture = pTexture;
	const Recti bounds(0, 0, pTexture->getSize().x, pTexture->getSize().y);
	setTextureRect(bounds);
}

SF2D_API void sf2d::comps::Sprite::setTextureRect(const Recti & texRect)
{
	KCHECK(m_vertexArray.getVertexCount() > 0);

	m_vertexArray[0].texCoords = Vec2f((float)(texRect.left), (float)(texRect.top));
	m_vertexArray[1].texCoords = Vec2f((float)(texRect.left + texRect.width), (float)(texRect.top));
	m_vertexArray[2].texCoords = Vec2f((float)(texRect.left + texRect.width), (float)(texRect.top + texRect.height));
	m_vertexArray[3].texCoords = Vec2f((float)(texRect.left), (float)(texRect.top + texRect.height));
}

Rectf Sprite::getOnscreenBounds() const
{
	return transform->getTransform().transformRect(Rectf(0, 0, m_size.x, m_size.y));
}

void Sprite::operator = (const Sprite& spr)
{
	m_vertexArray.clear(); //clear this classes vert arrayso it's empty 

	m_size = spr.m_size;
	m_vertexArray = spr.m_vertexArray;
	m_pTexture = spr.m_pTexture;

	transform = m_entity->transform;
}

void comps::Sprite::setSize(const Vec2f& size)
{
	m_vertexArray[0].position = Vec2f(0.0f, 0.0f);
	m_vertexArray[1].position = Vec2f(size.x, 0.0f);
	m_vertexArray[2].position = Vec2f(size.x, size.y);
	m_vertexArray[3].position = Vec2f(0.0f, size.y);
	m_size = size;
}
