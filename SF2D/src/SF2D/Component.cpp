#include <SF2D/Component.h>
#include <SF2D/Entity.h>

using namespace sf2d;

ComponentBase::ComponentBase(Entity * pEntity)
	: m_entity(pEntity), m_componentTag(GenerateUUID())
{
}
ComponentBase::~ComponentBase()
{
}

InitStatus ComponentBase::init()
{
	return InitStatus::Success;
}

void ComponentBase::cleanUp()
{
}

void ComponentBase::fixedTick()
{
}

void ComponentBase::tick()
{
}

void ComponentBase::onEnterScene()
{
}
void ComponentBase::onExitScene()
{
}
