#include <functional>
#include <stack>
#include <memory>

#include <SF2D/Collisions/CollisionOverlord.h>
#include <box2d/box2d.h>
#include <SF2D/Application.h>
#include <SF2D/Scene.h>

#include <SF2D/Physics/b2dConversion.h>

using namespace sf2d;
using namespace sf2d::collisions;
using namespace sf2d::comps;

b2AABB rectfToB2ABB(const Rectf& aabb)
{
	b2AABB converted;
	converted.lowerBound.x = aabb.left;
	converted.lowerBound.y = aabb.top;
	converted.upperBound.x = aabb.left + aabb.width;
	converted.upperBound.y = aabb.top + aabb.height;
	return converted;
}

CollisionOverlord::CollisionOverlord()
{
	m_pBroadPhase = std::make_unique<b2BroadPhase>();
	// Bind member functions used for collision 
	// detection
	m_collisionLookup[0][0] = std::bind(&CollisionOverlord::polyPoly, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	m_collisionLookup[0][1] = std::bind(&CollisionOverlord::polyCircle, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	m_collisionLookup[1][0] = std::bind(&CollisionOverlord::circlePoly, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	m_collisionLookup[1][1] = std::bind(&CollisionOverlord::circleCircle, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
}

CollisionOverlord::~CollisionOverlord()
{
	cleanupProxies();
}

void CollisionOverlord::triggerSceneCleanup()
{
	cleanupProxies();
}

void CollisionOverlord::triggerSceneConstruct()
{
	const uint32 ENTITY_COUNT = GET_SCENE()->getNumbrOfEntitiesAllocated();
	auto& entities = GET_SCENE()->getAllocatedEntityList();

	// Iterate over the list of all entites
	for (auto& e : entities)
	{
		// If the entity doesn't have a collider then it won't be added to the 
		// qtree
		auto colliders = e->getComponents<ColliderBase>();
		for (auto c : colliders)
		{
			if (!c)
			{
				continue;
			}

			ProxyInfo proxy;
			proxy.aabb = new b2AABB(rectfToB2ABB(c->getBoundingBox()));
			proxy.proxyId = m_pBroadPhase->CreateProxy(*proxy.aabb, nullptr);
			proxy.pEntity = e;
			proxy.pCollider = c;
			proxy.pToCheck = &m_intersectionsToCheck;

			m_proxies.push_back(proxy);
		}
	}
}

void CollisionOverlord::tick()
{
	relocateProxies();
	checkForProxyInteractions();
	performNarrowPhaseForProxies();
}

bool CollisionOverlord::doesTagMatchProxyId(const std::string& tag, int id) const
{
	const int32 idx = getProxyIndexFromId(id);
	if (idx == -1)
	{
		return false;
	}

	return m_proxies[idx].pEntity->getTag() == tag;
}

void sf2d::collisions::CollisionOverlord::castRayInScene(const Vec2f& start, const Vec2f& end, const RaycastCallback& cb, Entity* pCastingEntity)
{
	b2RayCastInput input;
	input.maxFraction = 1.0f;
	input.p1 = Vec2fTob2(start);
	input.p2 = Vec2fTob2(end);

	RaycastCBInternal cbInternal;
	cbInternal.pOverlord = this;
	cbInternal.pProxies = &m_proxies;
	cbInternal.externalCallback = &cb;
	if (pCastingEntity)
	{
		for (auto& p : m_proxies)
		{
			if (p.pEntity == pCastingEntity)
			{
				cbInternal.castingID = p.proxyId;
			}
		}
	}

	m_pBroadPhase->RayCast(&cbInternal, input);
}

void CollisionOverlord::cleanupProxies()
{
	for (auto& proxy : m_proxies)
	{
		proxy.pCollider = nullptr;
		if (proxy.aabb)
		{
			delete proxy.aabb;
			proxy.aabb = nullptr;
		}
	}
	m_proxies.clear();
}

void CollisionOverlord::relocateProxies()
{
	static bool bHasTickedOnce = false;
	for (auto& proxy : m_proxies)
	{
		if (!bHasTickedOnce)
		{
			proxy.lastPos = proxy.pEntity->transform->getTranslation();
		}
		else
		{
			const Vec2f currentPosition = proxy.pEntity->transform->getTranslation();
			const Vec2f displacement = currentPosition - proxy.lastPos;
			// if no displacement we can continue the main loop
			if (displacement == Vec2f(0.0f, 0.0f))
			{
				continue;
			}
			proxy.lastPos = currentPosition;

			(*proxy.aabb) = b2AABB(rectfToB2ABB(proxy.pCollider->getBoundingBox()));
			// Note MoveProxy returns a bool for if the object was reinserted
			// this may be useful within the confines of the world
			m_pBroadPhase->MoveProxy(proxy.proxyId, *proxy.aabb, Vec2fTob2(displacement));
		}
	}

	if (!bHasTickedOnce)
	{
		bHasTickedOnce = true;
	}
}

void CollisionOverlord::checkForProxyInteractions()
{
	m_intersectionsToCheck.clear();
	for (auto& p : m_proxies)
	{
		if (!p.pEntity->isActive())
		{
			continue;
		}
		m_pBroadPhase->TouchProxy(p.proxyId);
	}

	//struct cb {
	//	void AddPair(void*, void*) {};
	//};

	//cb test;

	//m_pBroadPhase->UpdatePairs(&test);

	for (auto& p : m_proxies)
	{
		m_pBroadPhase->Query(&p, *p.aabb);
	}
}

void CollisionOverlord::performNarrowPhaseForProxies()
{
	// We need the narrow phase queue to be populated before we do in depth searches
	generateNarrowPhaseQueue();

	for (auto pair : m_narrowPhaseQueue)
	{
		const ProxyInfo& proxyA = m_proxies[getProxyIndexFromId(pair.first)];
		const ProxyInfo& proxyB = m_proxies[getProxyIndexFromId(pair.second)];

		if (!proxyA.pEntity->isActive() || !proxyB.pEntity->isActive())
			continue;

		// Need as raw pointer since box2d uses raw
		b2Shape* pShapeA = proxyA.pCollider->getB2Shape().lock().get();
		b2Shape* pShapeB = proxyB.pCollider->getB2Shape().lock().get();

		b2Transform transA = proxyA.pCollider->getB2Transform();
		b2Transform transB = proxyB.pCollider->getB2Transform();

		const int32 colliderTypeA = static_cast<int32>(proxyA.pCollider->getColliderType());
		const int32 colliderTypeB = static_cast<int32>(proxyB.pCollider->getColliderType());

		CollisionDetectionData d;
		//d.entityA = proxyA.pEntity;
		//d.entityB = proxyB.pEntity;

		/*	const auto collision = CollisionLookupTable[colliderTypeA][colliderTypeB](d);
			if (!collision)
			{
				continue;
			}*/

		auto collision = b2TestOverlap(pShapeA, 0, pShapeB, 0, transA, transB);
		if (!collision)
		{
			continue;
		}


		CollisionDetectionData data;

		m_collisionLookup[colliderTypeA][colliderTypeB](proxyA, transA, proxyB, transB, data);

		if (data.contactCount == 0)
		{
			continue;
		}

		//data.entityA = proxyA.pEntity;
		//data.entityB = proxyB.pEntity;
		data.collidedWith = proxyB.pEntity;
		proxyA.pCollider->collisionCallback(data);

		data.collisionNormal = -data.collisionNormal;
		data.collidedWith = proxyA.pEntity;
		//data.penetration = -data.penetration;
		proxyB.pCollider->collisionCallback(data);
	}
}

void CollisionOverlord::generateNarrowPhaseQueue()
{
	m_narrowPhaseQueue.clear();
	for (auto& pair : m_intersectionsToCheck)
	{

		const int32 indexA = getProxyIndexFromId(pair.first);
		const int32 indexB = getProxyIndexFromId(pair.second);

		auto pColliderA = m_proxies[indexA].pCollider;
		auto pColliderB = m_proxies[indexB].pCollider;

		const ColliderFilteringData& filterA = pColliderA->getCollisionFilteringData();
		const ColliderFilteringData& filterB = pColliderB->getCollisionFilteringData();

		// if collision filters tested against collision masks for entity a & b exclude them from being allowed
		// to collide with one another, then continue onto the next pair.

		if ((filterA.collisionFilter & filterB.collisionMask) == 0 || (filterB.collisionFilter & filterA.collisionMask) == 0)
		{
			continue;
		}

		// If an entitiy has multiple colliders, prevent those colliders
		// registering a collision with one another
		if (m_proxies[indexA].pEntity == m_proxies[indexB].pEntity)
		{
			continue;
		}

		const auto result = std::find_if(m_narrowPhaseQueue.begin(), m_narrowPhaseQueue.end(), [pair](const ProxyPair& toCheckPair) -> bool
			{
				if (pair == toCheckPair)
				{
					return true;
				}

				if (pair == ProxyPair(toCheckPair.second, toCheckPair.first))
				{
					return true;
				}

				return false;
			});

		if (result == m_narrowPhaseQueue.end())
		{
			m_narrowPhaseQueue.push_back(pair);
		}
	}
}

int32 CollisionOverlord::getProxyIndexFromId(int32 proxyId) const
{
	const auto result = std::find_if(m_proxies.begin(), m_proxies.end(), [proxyId](const ProxyInfo& p) -> bool
		{
			return proxyId == p.proxyId;
		});

	if (result == m_proxies.end())
	{
		return -1;
	}

	return KCAST(int32, std::distance(m_proxies.begin(), result));
}

void CollisionOverlord::circleCircle(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data)
{
	b2Manifold manifold;
	auto shapeA = proxyA.pCollider->getB2Shape().lock();
	auto shapeB = proxyB.pCollider->getB2Shape().lock();

	auto circleA = std::dynamic_pointer_cast<b2CircleShape>(shapeA);
	auto circleB = std::dynamic_pointer_cast<b2CircleShape>(shapeB);


	b2CollideCircles(&manifold, circleA.get(), transformA, circleB.get(), transformB);

	b2WorldManifold wm;
	wm.Initialize(&manifold, transformA, circleA->m_radius, transformB, circleB->m_radius);

	data.collisionNormal = b2ToVec2f(wm.normal);

	for (int32 i = 0; i < 2; ++i)
	{
		data.contacts[i] = b2ToVec2f(wm.points[i]);
	}
	data.contactCount = manifold.pointCount;
	data.contactCount = manifold.pointCount;
	data.penetration = wm.separations[0];
}

void CollisionOverlord::polyCircle(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data)
{
	b2Manifold manifold;
	auto shapeA = proxyA.pCollider->getB2Shape().lock();
	auto shapeB = proxyB.pCollider->getB2Shape().lock();

	auto poly = std::dynamic_pointer_cast<b2PolygonShape>(shapeA);
	auto circle = std::dynamic_pointer_cast<b2CircleShape>(shapeB);

	b2CollidePolygonAndCircle(&manifold, poly.get(), transformA, circle.get(), transformB);

	b2WorldManifold wm;
	wm.Initialize(&manifold, transformA, poly->m_radius, transformB, circle->m_radius);

	data.collisionNormal = b2ToVec2f(wm.normal);
	for (int32 i = 0; i < 2; ++i)
	{
		data.contacts[i] = b2ToVec2f(wm.points[i]);
	}
	data.contactCount = 1;
	data.contactCount = manifold.pointCount;
	data.penetration = wm.separations[0];
}

void CollisionOverlord::circlePoly(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data)
{
	b2Manifold manifold;

	auto shapeA = proxyA.pCollider->getB2Shape().lock();
	auto shapeB = proxyB.pCollider->getB2Shape().lock();

	auto circle = std::dynamic_pointer_cast<b2CircleShape>(shapeA);
	auto poly = std::dynamic_pointer_cast<b2PolygonShape>(shapeB);

	b2CollidePolygonAndCircle(&manifold, poly.get(), transformA, circle.get(), transformB);

	b2WorldManifold wm;
	wm.Initialize(&manifold, transformA, poly->m_radius, transformB, circle->m_radius);

	data.collisionNormal = b2ToVec2f(wm.normal);
	for (int32 i = 0; i < 2; ++i)
	{
		data.contacts[i] = b2ToVec2f(wm.points[i]);
	}
	data.contactCount = 1;
	data.contactCount = manifold.pointCount;
	data.penetration = wm.separations[0];
}

void CollisionOverlord::polyPoly(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data)
{
	b2Manifold manifold;
	auto shapeA = proxyA.pCollider->getB2Shape().lock();
	auto shapeB = proxyB.pCollider->getB2Shape().lock();

	auto polyA = std::dynamic_pointer_cast<b2PolygonShape>(shapeA);
	auto polyB = std::dynamic_pointer_cast<b2PolygonShape>(shapeB);

	b2CollidePolygons(&manifold, polyA.get(), transformA, polyA.get(), transformB);
	b2WorldManifold wm;
	wm.Initialize(&manifold, transformA, polyA->m_radius, transformB, polyB->m_radius);

	data.collisionNormal = -b2ToVec2f(wm.normal);

	for (int32 i = 0; i < 2; ++i)
	{
		data.contacts[i] = b2ToVec2f(wm.points[i]);
	}
	data.contactCount = 2;
	data.contactCount = manifold.pointCount;
	data.penetration = wm.separations[0];
}

float CollisionOverlord::RaycastCBInternal::RayCastCallback(const b2RayCastInput& input, int id)
{
	int32 proxyIdx = pOverlord->getProxyIndexFromId(id);

	Entity* const pEntity = (*pProxies)[proxyIdx].pEntity; // The entity we potentially intersected with
	
	// Ignore none-active entities
	if (!pEntity->isActive())
	{
		return 1.0f;
	}

	//auto colliderList = pEntity->getComponents<ColliderBase>();
	auto pCollider = (*pProxies)[proxyIdx].pCollider;

	// If there was a casting ray entity that we would like to ignore, then continue the ray
	if (id == castingID)
	{
		return 1.0f;
	}

	/*
	There is a couple downsides here to how we're performing the raycast.
	- The embedded callback system is kind of obtuese and difficult to follow when debugging if/when something goes wrong
	- Because 1 entity can store multiple colliders we end up having to iterate over all it's colliders and checking the raycast,
	which feels like it mitigates the benefit of doing the {dynamic-tree raycast -> collider raycast -> detection callback} algorithm
	- May be a better approach to add only allow 1 collider, but allow child shapes to the collider
	*/

	/* 
	Would also like 2 types of raycast function:
	- 1 that reports the first hit
	- 1 that calls a callback for all shapes along its path
	*/

	b2RayCastOutput shapeRaycastOutput;
	bool bShouldContinueRay = true;
	//for (auto& c : colliderList)
	std::weak_ptr<b2Shape> box2DShape = pCollider->getB2Shape();
	const bool bDidIntersect = box2DShape.lock()->RayCast(&shapeRaycastOutput, input, pCollider->getB2Transform(), 0);
	// No intersection, continue to the next collider
	if (!bDidIntersect)
	{
		return 1.0f;
	}

	RaycastCallbackData cb;
	cb.pEntity = pEntity;
	cb.normal = b2ToVec2f(shapeRaycastOutput.normal);
	cb.fraction = shapeRaycastOutput.fraction;
	cb.pCollider = pCollider;
	bShouldContinueRay = (*externalCallback)(cb);

	return bShouldContinueRay ? 1.0f : 0.0f;
}
