
#include <SF2D/SF2D.h>
#include <SF2D/Application.h>
#include <SF2D/AssetLoader/AssetLoader.h>


#include <future>
#include <SFML/Graphics/VertexBuffer.hpp>

#pragma comment(lib, "rpcrt4.lib")  // UuidCreate - Minimum supported OS Win 2000

#ifdef _WIN32
#include <rpc.h>
#include <Windows.h>

#endif

using namespace sf2d;
using namespace sf2d::maths;


InitStatus sf2d::StartupEngine(AppInit* windowInit)
{

	Application* const app = Application::getApp();
	app->setupApplication(*windowInit);
	AssetLoader::getAssetLoader();

	InitRand();

	return InitStatus::Success;
}

SF2D_API InitStatus sf2d::InitialiseSubmodules()
{
	if (!sf::VertexBuffer::isAvailable())
	{
		spdlog::error("ERROR! VertexBuffers are not available on this machine!");
		return InitStatus::Failure;
	}

	if (!sf::Shader::isAvailable())
	{
		spdlog::error("ERROR! Shaders are not available on this machine!");
		return InitStatus::Failure;
	}


	return Application::getApp()->initialiseScenes();
}

void sf2d::ShutdownEngine()
{
	AssetLoader::getAssetLoader().cleanupAssetLoader();
	// Cleanup applicaiton
	auto app = Application::getApp();
	app->cleanupApplication();
	KFREE(app);
}

void sf2d::RunApplication()
{
	auto app = Application::getApp();

	app->runApplication();
}

std::string sf2d::GenerateUUID()
{
	UUID uuid;
	UuidCreate(&uuid);
	char* uuid_cstr = nullptr;
	UuidToStringA(&uuid, reinterpret_cast<RPC_CSTR*>(&uuid_cstr));
	auto res = std::string(uuid_cstr);
	RpcStringFreeA(reinterpret_cast<RPC_CSTR*>(&uuid_cstr));
	return res;
}
