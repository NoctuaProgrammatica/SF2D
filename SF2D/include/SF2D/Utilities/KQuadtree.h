#ifndef KQUADTREE
#define KQUADTREE

#include <vector>
#include <stack> 

#include <SF2D/SF2D.h>
#include <SF2D/Entity.h>

namespace sf2d
{
	enum LeavesIdentifier : int32
	{
		noLeaf = -1,
		northWest,
		northEast,
		southWest,
		southEast,
		leavesIdentifierCount
	};

	class KQuadtree
	{
	public:

		KQuadtree(int level, const Rectf& bounds)
			: m_level(level), m_boundary(bounds), m_bHasSubdivided(false), m_leaves{ nullptr }
		{
		}

		~KQuadtree()
		{

		}

		bool insert(Entity* p);
		void getPossibleCollisions(Entity* pEntity, std::stack<Entity*>& entityStack);
		void clear();

	private:

		void subdivide();
		LeavesIdentifier getLeafEnum(Entity* pEntity) const;
		std::stack<LeavesIdentifier> getLeavesEnum(Entity* pEntity) const;

		constexpr static int MAX_ENTITIES = 10;
		constexpr static int MAX_NUM_LEVELS = CHUNK_POOL_SIZE / MAX_ENTITIES;

		KQuadtree* m_leaves[LeavesIdentifier::leavesIdentifierCount];

		Rectf m_boundary;

		std::vector<Entity*> m_nodeVector;
		std::vector<Entity*> m_queriedPointList;

		std::stack<Entity*> m_queriedEntityStack;

		int m_level;
		bool m_bHasSubdivided;

	};
}

#endif 
