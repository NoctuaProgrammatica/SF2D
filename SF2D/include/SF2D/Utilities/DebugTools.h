#pragma once 

#include <chrono>
#include <string>
#include <SF2D/SF2DLib.h>
#include <SF2D/Utilities/EngineMacros.h>


namespace sf2d
{
	namespace profile
	{
		SF2D_API inline std::chrono::high_resolution_clock::time_point StartFunctionTimer();

		SF2D_API inline long long EndFunctionTimer(const std::chrono::high_resolution_clock::time_point&  t1, const std::string& funcName, bool bIsMicroSeconds = true, bool logFile = true);
	}
}
