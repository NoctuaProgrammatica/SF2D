#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/SF2DLib.h>
#include <SF2D/Component.h>

#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace sf2d
{
	namespace comps
	{
		class RenderableBase :
			public ComponentBase, public sf::Drawable
		{
		public:

			SF2D_API RenderableBase(sf2d::Entity* pEntity);
			SF2D_API virtual ~RenderableBase() = default;


			SF2D_API void setRenderLayer(sf2d::int32 renderLayer) { m_renderLayer = renderLayer; }
			SF2D_API sf2d::int32 getRenderLayer() const { return m_renderLayer; }
			SF2D_API sf::Shader* getShader() const { return m_pShader; }
			SF2D_API void setShader(sf::Shader* pShader) { m_pShader = pShader; }
			SF2D_API virtual void postRenderEvent() { return;  }
			SF2D_API virtual sf2d::Rectf getOnscreenBounds() const = 0;

		private:

			sf2d::int32 m_renderLayer = 0;
			sf::Shader* m_pShader = nullptr;
		};
	}
}
