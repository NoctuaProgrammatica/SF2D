#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Components/ColliderBase.h>

class b2PolygonShape;

namespace sf2d
{
	namespace comps
	{
		class Transform;
		class BoxCollider : public ColliderBase
		{
		public:

			SF2D_API BoxCollider(Entity* pEntity, const Vec2f& size, bool bUseParentLocation = true);
			SF2D_API ~BoxCollider() = default;

			SF2D_API KDEPRECATED_FUNC(const Rectf& getBounds)();
			SF2D_API virtual const Rectf& getBoundingBox() override;
			SF2D_API const Vec2f& getHalfSize() const { return m_halfSize; }
			SF2D_API	Vec2f getTopLeftCoord() const;

			SF2D_API virtual void setOverridePosition(const Vec2f& pos) override
			{
				ColliderBase::setOverridePosition(pos);
				updateAABB();
			}

		private:

			void updateAABB();

			Vec2f m_size;
			Vec2f m_halfSize;
			Rectf m_aabb;

			std::weak_ptr<b2PolygonShape> m_pBoxShapeb2;

			Transform* transform;
		};
	}
}
