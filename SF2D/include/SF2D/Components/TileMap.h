#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Component.h>
#include <SF2D/Components/RenderableBase.h>
#include <SF2D/Entity.h>

#include <SF2D/Tiled/TiledImport.h>
#include <SFML/Graphics/VertexBuffer.hpp>

#include <vector>

namespace sf2d
{
	namespace comps
	{
		enum class TileStateEnum : int32
		{
			Walkable,
			Slowdown,
			Impassable
		};

		/*
		With a split TileMap the map is drawn as individual horizontal lines
		each line may contain multiple layers rendered after another.

		*/
		struct HorizontalTileLine : RenderableBase
		{
			HorizontalTileLine()
				: RenderableBase(nullptr), horizontalGridSize(0), pTexture(nullptr)
			{
			}

			virtual void draw(sf::RenderTarget& target, sf::RenderStates) const override;

			SF2D_API sf2d::Rectf getOnscreenBounds() const
			{
				return Rectf(pTransform->transformPoint(topLeft), Vec2f((float)horizontalGridSize * pTileset.tileWidth, (float)pTileset.tileHeight));
			};

			std::vector<sf::VertexBuffer> vertexBuffersByLayerVector;
			const sf::Transform* pTransform = nullptr;
			int32 horizontalGridSize;
			Vec2f topLeft;
			sf::Texture* pTexture;
			tiled::Tileset pTileset;
		};

		class TileMapSplit : public RenderableBase
		{
		public:

			SF2D_API TileMapSplit(Entity* pEntity, const std::string& tiledMapName);
			SF2D_API ~TileMapSplit() = default;

			SF2D_API virtual InitStatus init() override;
			SF2D_API virtual void cleanUp() override;
			SF2D_API virtual void draw(sf::RenderTarget& target, sf::RenderStates) const override;
			SF2D_API const tiled::MapRoot* const getTiledMapImportData() const { return m_pTiledImportData; }

			SF2D_API virtual Rectf getOnscreenBounds() const override;
			SF2D_API std::vector<HorizontalTileLine>& getHorizontalTileLines() { return m_tileMapVec; }
		private:

			void isolateBlockedMap();

			tiled::MapRoot* m_pTiledImportData;

			Vec2i m_gridDimensions;
			Vec2i m_tileDimensions;

			std::vector<HorizontalTileLine> m_tileMapVec;
			std::vector<sf::VertexBuffer> m_preDrawLayers;
			std::vector<TileStateEnum> m_tileEnumStatesVector;

			const Transform* m_pTransformComponent;
			sf::Texture* m_pTexture;


		};

		class TileMap : public RenderableBase
		{
		public:

			SF2D_API TileMap(Entity* pEntity, const std::string& tiledMapName);
			SF2D_API ~TileMap() = default;

			virtual void draw(sf::RenderTarget&, sf::RenderStates) const override;
			SF2D_API Rectf getOnscreenBounds() const override { return Rectf{}; }
			SF2D_API virtual InitStatus init() override;

			SF2D_API const tiled::MapRoot* const getTiledMapImportData() const { return m_pTiledImportData; }


		private:

			std::vector<sf::VertexBuffer> m_layerVertexBufferVector;
			std::vector<sf::Texture*> m_texturesVector;
			std::vector<TileStateEnum> m_tileStates;

			tiled::MapRoot* m_pTiledImportData;
			const Transform* m_pTransformComponent;
			Vec2i m_gridDimensions;
			Vec2i m_tileDimensions;

		};
	}
}
