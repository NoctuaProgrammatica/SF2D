#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Component.h>
#include <SF2D/Entity.h>
#include <SFML/Graphics/Transform.hpp>

namespace sf2d
{
	namespace comps
	{
		/*
		World & local transform manipulating component.
		All entities are given a transform component upon construction.

		Use case:
		int main()
		{
		Entity entity;
		auto pTransform = entity.getComponent<Transform>();
		KCHECK(pTransform);
		pTransform->setScale(1.0f, 1.0f); // will set entity

		return 0;
		}
		*/
		class Transform : public ComponentBase
		{
		public:

			//Param: Pointer to entity transform is attatched to 
			//Return: n/a 
			//Info: 
			SF2D_API Transform(Entity* pEntity);
			SF2D_API Transform(Transform& toCopy);
			SF2D_API void operator = (Transform& toCopy);
			SF2D_API ~Transform() = default;

			//Param: n/a
			//Return: n/a
			//Info: Will update the entire transform object 
			SF2D_API virtual void tick() override;

			//Param: n/a 
			//Return: Transform
			//Info: Returns a reference to the internally stored transform entity
			SF2D_API const sf::Transform& getTransform() const;

			//Return: n/a
			//Info: Set the entitiy this transform is parented to 
			SF2D_API void setParent(Entity* pEntity);

			//Param: float angle of rotation 
			//Return: n/a 
			//Info: set the local rotation of the entity (in degrees)
			SF2D_API void setRotation(float angleInDeg);

			//Param: Vec2f origin point/float x, float y
			//Return: n/a
			//Info: set the origin for rotation and scale and translation (local space to the entity)
			SF2D_API void setOrigin(const Vec2f& origin);
			SF2D_API void setOrigin(float x, float y);

			//Param: Vec2f translation/float transX, transY
			//Return: n/a
			//Info: set translation of entity 
			SF2D_API void setPosition(const Vec2f& trans);
			SF2D_API void setPosition(float dx, float dy);

			//Param: n/a
			//Return: Vec2f position
			//Info: get world position of entity
			SF2D_API const Vec2f getPosition() const;

			//Param: n/a 
			//Return: float rotation 
			//Info: get world rotation of entity (in degrees)
			SF2D_API float getRotation() const;

			//Param: n/a 
			//Return: vec2f origin
			//Info: get local origin of entity (in degrees)
			SF2D_API const Vec2f& getOrigin()const;

			//Param: Vec2f scale/float scaleX, float scale Y
			//Return: n/a
			//Info: set scale of entity  
			SF2D_API void setScale(const Vec2f& scale);
			SF2D_API void setScale(float x, float y);

			//Param: n/a
			//Return: Vec2f scale
			//Info: get scale of entity
			SF2D_API Vec2f getScale() const;

			//Param: Vec2f moveVector/float xMove, yMove
			//Return: n/a 
			//Info: Translate the entity by Vector(xMove,yMove)
			SF2D_API void move(float dx, float dy);
			SF2D_API void move(const Vec2f& trans);

			//Param: float angle 
			//Return: n/a
			//Info: rotate the entity by angle passed (in degrees)
			SF2D_API void rotate(float angleInDeg);

			//Param: Vec2f scale/scale x, scale y 
			//Return: n/a
			//Info: scale the entity by vec(scaleX,scaleY)
			SF2D_API void scale(const Vec2f& scale);
			SF2D_API void scale(float sx, float sy);

			SF2D_API const Vec2f& getTranslation() const { return m_trans; }

		private:

			void reconstructTransform();

			Transform* m_pParentTransform;

			sf::Transform m_transform;
			sf::Transform m_combinedWithParentTransform;

			float m_rotation;

			Vec2f m_origin;
			Vec2f m_scale;
			Vec2f m_trans;

			Vec2f m_worldPos;
			Vec2f m_worldScale;
			float m_worldRot;

			bool m_bHasParent;
			bool m_bUpdateTransform;
		};
	}
}
