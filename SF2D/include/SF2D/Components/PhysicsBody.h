#pragma once 

// KRAWLER Includes
#include <SF2D/SF2D.h>
#include <SF2D/Component.h>

// Box2D Includes
#include <box2d/box2d.h>

namespace sf2d
{
	namespace comps
	{

		enum class BodyType : int32
		{
			Static_Body = 0,
			Kinematic_Body,
			Dynamic_Body
		};
		/*
		Physics Body Definition
		*/
		struct BodyDef
		{
			BodyDef() :
				bBullet(false), gravityScale(1.0f), bAwake(true),
				bodyType(BodyType::Static_Body), angle(0.0f),
				linearVelocity(0.0f, 0.0f), angularVelocity(0.0f),
				bFixedRotation(false), linearDamping(0.0f),
				angularDamping(0.0f), bActive(true), bAllowSleep(true)

			{
			}
			~BodyDef() = default;
			bool bBullet;
			float gravityScale;
			bool bAwake;
			bool bAllowSleep;
			BodyType bodyType;
			float angle;
			Vec2f position;
			Vec2f linearVelocity;
			float angularVelocity;
			bool bActive;
			bool bFixedRotation;
			float linearDamping;
			float angularDamping;

		};

		/*
		Physics Material Definition
		NOTE:
		The following from Box2D's 'b2Fixture Def' have not been
		translated into KRAWLER:
		- isSensor
		- collision filtering
		- shape
		- userData (may not support)
		*/
		struct MatDef
		{
			MatDef()
				: density(0.0f), friction(0.1f), restitution(0.0f)
			{
			}
			~MatDef() = default;

			float density;
			float friction;
			float restitution;
		};

		class PhysicsBody :
			public ComponentBase
		{
		public:

			SF2D_API PhysicsBody(Entity& entity, const Vec2f& bounds);
			SF2D_API PhysicsBody(Entity& entity, const Vec2f& bounds, const BodyDef& bodyDef);
			SF2D_API PhysicsBody(Entity& entity, const Vec2f& bounds, const BodyDef& bodyDef, const MatDef& matDef);

			SF2D_API ~PhysicsBody() = default;

			// Overrides from component base
			SF2D_API virtual InitStatus init() override;
			SF2D_API virtual void fixedTick() override;
			SF2D_API virtual void onEnterScene() override;
			SF2D_API virtual void onExitScene() override;

			// @return Material definition this body was created with
			SF2D_API const MatDef& getMaterialDefinition() const { return m_matDef; }

			// @return Body definition this body was created with
			SF2D_API const BodyDef& getBodyDefinition() const { return m_bodyDef; }

			// @param new position
			SF2D_API void setPosition(const Vec2f& position) const;

			// @param Rotation in radians,
			SF2D_API void setRotation(float rotation) const;

			// @param Is the body active in simulations
			SF2D_API void setActivity(bool bIsActive) const;

			/// @return the linear velocity of the center of mass.
			SF2D_API Vec2f getLinearVelocity() const;

			/// @param v the new linear velocity of the center of mass.
			SF2D_API void setLinearVelocity(const Vec2f& velocity);

			/// @return the angular velocity in radians/second.
			SF2D_API float getAngularVelocity() const;

			/// @param omega the new angular velocity in radians/second.
			SF2D_API void setAngularVelocity(float velocity);

			/// @param force the world force vector, usually in Newtons (N).
			/// @param point the world position of the point of application.
			/// @param wake also wake up the body
			SF2D_API void applyForce(const Vec2f& force, const Vec2f& point, bool wake = true);

			/// @param force the world force vector, usually in Newtons (N).
			/// @param wake also wake up the body
			SF2D_API void applyForceToCentre(const Vec2f& force, bool wake = true);

			/// @param torque about the z-axis (out of the screen), usually in N-m.
			/// @param wake also wake up the body
			SF2D_API void applyTorque(float torque, bool wake = true);

			/// @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
			/// @param point the world position of the point of application.
			/// @param wake also wake up the body
			SF2D_API void applyLinearImpulse(const Vec2f& impulse, const Vec2f& point, bool wake = true);

			/// @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
			/// @param wake also wake up the body
			SF2D_API void applyLinearImpulseToCenter(const Vec2f& impulse, bool wake = true);

			/// @return the mass, usually in kilograms (kg).
			SF2D_API void getMass() const;

			/// Get the gravity scale of the body.
			SF2D_API float getGravityScale() const;

			/// Set the gravity scale of the body.
			SF2D_API void setGravityScale(float scale = 1.0f);

			// @param Density in Kg/m^2
			SF2D_API void setDensity(float density);

		private:
			BodyDef m_bodyDef;
			MatDef m_matDef;

			b2PolygonShape m_polygonShape;

			Vec2f m_bounds;
			Vec2f m_halfBounds;

			b2Body* m_pB2Body = nullptr;
			b2Fixture* m_pCurrentFixture = nullptr;
		};
	}
}
