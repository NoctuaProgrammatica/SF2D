#pragma once

#include <SF2D/SF2D.h>
#include <box2d/box2d.h>

// Forward Declerations
class b2World;

namespace sf2d
{
	namespace physics
	{

		class PhysicsWorld2D
		{
		public:

			SF2D_API PhysicsWorld2D();
			SF2D_API ~PhysicsWorld2D();

			SF2D_API sf2d::InitStatus initialiseWorld();
			SF2D_API void cleanupWorld();

			SF2D_API	void stepWorld(float physicsStep);
			SF2D_API void setGravity(const sf2d::Vec2f& g);
			SF2D_API sf2d::Vec2f getGravity() const;

			// BOX2D INTERACTION FUNCTIONALITY
			b2Body* addNewBody(const b2BodyDef& def);
			void removeBody(b2Body* const pBody);

			SF2D_API void rayCast(const Vec2f& start, const Vec2f& end);

			SF2D_API void setPPM(float ppm);

			SF2D_API float getPPM() const { return m_ppm; }

		private:

			struct RaycastCB : public b2RayCastCallback
			{
				virtual float ReportFixture(b2Fixture* fixture, const b2Vec2& point,
					const b2Vec2& normal, float fraction) override
				{
					spdlog::debug("Found intersection on raycast with fraction of {}", fraction);
					return 0.0f;
				}

			};

			b2World* m_pBox2DWorld = nullptr;
			sf2d::Vec2f m_gravity = sf2d::Vec2f(0, 9.81f);
			int32 m_velocityIterations = 8;
			int32 m_positionIterations = 3;

			float m_ppm = 1.0f;
		};
	}
}
