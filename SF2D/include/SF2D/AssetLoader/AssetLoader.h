#pragma once 

#include <SF2D/SF2D.h>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SF2D/Tiled/TiledImport.h>

#include <unordered_map>

#ifndef ASSET
#define ASSET() AssetLoader::getAssetLoader()
#endif 

namespace sf2d
{
	namespace anim
	{
		struct AnimationDef;
	};

	class AssetLoader
	{
	public:
		SF2D_API ~AssetLoader();

		SF2D_API static AssetLoader& getAssetLoader()
		{
			static AssetLoader* pApplication = new AssetLoader();

			return *pApplication;
		}

		SF2D_API void cleanupAssetLoader();

		SF2D_API sf::Texture* const getTexture(const std::string& name);
		SF2D_API sf::SoundBuffer* const getSound(const std::string& name);
		SF2D_API sf::Shader* const getShader(const std::string& name);
		SF2D_API sf::Font* const getFont(const std::string& name);
		SF2D_API sf::Music* const getMusic(const std::string& name);
		SF2D_API anim::AnimationDef* const getAnimation(const std::string& name);
		SF2D_API tiled::MapRoot* const getLevelMap(const std::string& name);

		SF2D_API void setRootFolder(const std::string& rootFolder) { m_rootFolder = rootFolder; }


	private:

		AssetLoader();
		void loadAssetXML();
		void loadAnimationsXML();
		void matchAnimationsToTextures();

		const std::string XML_FILE = "assets.xml";

		std::string m_rootFolder;

		void loadTexture(const std::string& name, const std::string& filePath);
		void loadShader(const std::string& shaderName, const std::string& vertPath, const std::string fragPath);
		void loadSound(const std::string& name, const std::string& filePath);
		void loadFont(const std::string& name, const std::string& filePath);
		void loadTilemap(const std::string& name, const std::string& filePath);
		void loadMusic(const std::string& name, const std::string& filePath);

		std::unordered_map <std::string, sf::Texture*> m_texturesMap;
		std::unordered_map <std::string, sf::Font*> m_fontMap;
		std::unordered_map <std::string, sf::SoundBuffer*> m_soundBufferMap;
		std::unordered_map <std::string, sf::Shader*> m_shaderMap;
		std::unordered_map <std::string, sf::Music*> m_musicFilesMap;
		std::unordered_map <std::string, anim::AnimationDef*> m_animationsMap;
		std::unordered_map <std::string, tiled::MapRoot*> m_importedLevelsMap;
	};
}
