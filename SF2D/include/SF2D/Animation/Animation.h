#pragma once

#include <SF2D/SF2D.h>

#include <vector>
#include <SFML/Graphics/Texture.hpp>

namespace sf2d
{
	namespace anim
	{
		struct AnimationDef
		{
			float frameTime; // Amount of time to elapse between frames
			std::string animationName; //name of animation to engine
			std::string textureName; // name of texture bound to this animation
			sf::Texture* pTexture = nullptr;
			sf2d::Vec2f bounds; //width & height for each sub-image
			std::vector<sf2d::Vec2f> frameData; //vector of all animations
		};

	}
}
