#pragma once 

#include <SF2D/SF2D.h>

//STL includes
#include <set>
#include <string>

//SFML includes
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Joystick.hpp>

//TODO change set to list

namespace sf2d
{
	namespace input
	{
		using KKey = sf::Keyboard::Key;
		using KMouseButton = sf::Mouse::Button;
		
		class InputHandler
		{
		public:

			SF2D_API static void SetWindow(sf::RenderWindow* window);
			SF2D_API static void Update();
			SF2D_API static void HandleEvent(const sf::Event& evt);

			SF2D_API static bool JustPressed(sf::Keyboard::Key key); //TODO Prevent repeat key firing
			SF2D_API static bool Pressed(sf::Keyboard::Key key);
			SF2D_API static bool JustReleased(sf::Keyboard::Key key);
			SF2D_API static bool MouseJustPressed(sf::Mouse::Button button);
			SF2D_API static bool MousePressed(sf::Mouse::Button button);
			SF2D_API static bool MouseJustReleased(sf::Mouse::Button button);
			SF2D_API static bool JoystickJustPressed(unsigned int button);
			SF2D_API static bool JoystickJustReleased(unsigned int button);

			SF2D_API static void SetMouseLocked(bool mouseLocked);

			// Returns mouse position in screen space
			SF2D_API static sf2d::Vec2i GetMousePosition() { return m_mousePosition; }
			
			// Returns mouse move delta
			SF2D_API static sf2d::Vec2i GetMouseDelta() { return m_mouseDelta; }
			
			// Returns mouse position in world space
			SF2D_API static sf2d::Vec2f GetMouseWorldPosition();

			SF2D_API static float GetMouseScrollDelta() { return m_mouseScrollDelta; }

			SF2D_API static std::string GetTextEntered() { return m_textEntered; }

		private:
			static void EventKeyPressed(sf::Keyboard::Key key);
			static void EventKeyReleased(sf::Keyboard::Key key);
			static void EventMouseButtonPressed(sf::Mouse::Button button);
			static void EventMouseButtonReleased(sf::Mouse::Button button);
			static void EventMouseScrollMoved(float delta);
			static void EventTextEntered(sf::Uint32 charCode);
			static void EventJoystickButtonPressed(unsigned int);
			static void EventJoystickButtonReleased(unsigned int);

			static sf::RenderWindow* mp_window;

			static std::set<sf::Keyboard::Key> m_keysJustPressed;
			static std::set<sf::Keyboard::Key> m_keysPressed;
			static std::set<sf::Keyboard::Key> m_keysJustReleased;

			static sf::Vector2i m_mousePosition;
			static sf::Vector2i m_mouseDelta;

			static std::set<sf::Mouse::Button> m_mouseJustPressed;
			static std::set<sf::Mouse::Button> m_mousePressed;
			static std::set<sf::Mouse::Button> m_mouseJustReleased;

			static std::set<unsigned int> m_joystickJustPressed;
			static std::set<unsigned int> m_joystickJustReleased;

			static std::string m_textEntered;

			static float m_mouseScrollDelta;

			static bool mb_mouseLocked;
		};
	}
}
